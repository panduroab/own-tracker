(function(window, undefined) {
    var Tracker = {};

    function loadScript(url, callback) {
        var script = document.createElement('script');
        script.async = true;
        script.src = url;
        var entry = document.getElementsByTagName('script')[0];
        entry.parentNode.insertBefore(script, entry);
        script.onload = script.onreadystatechange = function() {
            var ready = script.readyState;
            if (!ready || /complet|loaded/.test(script.readyState)) {
                callback();
                script.onload = null;
                script.onreadystatechange = null;
            }
        };
    }

    function getLocation() {
        loadScript('http://j.maxmind.com/app/geoip.js', function() {
            var info = {
                "Location": {
                    "Country Code": geoip_country_code(),
                    "Country Name": geoip_country_name(),
                    "City": geoip_city(),
                    "Region": geoip_region(),
                    "Region Name": geoip_region_name(),
                    "Latitude": geoip_latitude(),
                    "Longitude": geoip_longitude(),
                    "Postal Code": geoip_postal_code()},
                "UserAgent": {
                    "Browser CodeName": navigator.appCodeName,
                    "Browser Name": navigator.appName,
                    "Browser Version": navigator.appVersion,
                    "Cookies Enabled": navigator.cookieEnabled,
                    "Browser Language": navigator.language,
                    "Browser Online": navigator.onLine,
                    "Platform": navigator.platform,
                    "User-agent header": navigator.userAgent,
                    "User-agent language": navigator.systemLanguage
                }
            };
            alert(JSON.stringify(info.UserAgent));
        });
    }

    getLocation();


    return Tracker;
})(this);